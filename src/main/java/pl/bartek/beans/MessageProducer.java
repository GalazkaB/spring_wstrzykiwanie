package pl.bartek.beans;


public interface MessageProducer {

	public String getMessage();
}